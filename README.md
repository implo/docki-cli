# Docki

## Preview

![screencast](.gitlab/screencast.gif)

Docki is cli for converting asciidoctor files into html files. You can build your documentation with `docki build` and write documenation using the live server with `docki serve`.

## Installation

### Homebrew

```
brew tap quirinecker/docki-homebrew https://gitlab.com/quirinecker/docki-homebrew
```

```
brew install docki 
```

### Cargo

```shell
cargo install docki
```



